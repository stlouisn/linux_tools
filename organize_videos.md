### Organize Videos

##### as owner:

```bash
exiftool '-FileName<CreateDate' -d 'VID_%Y%m%d_%H%M%S.%%e' vid_*
```

##### with sudo:

```bash
sudo exiftool '-FileName<CreateDate' -d 'VID_%Y%m%d_%H%M%S.%%e' vid_*
```
