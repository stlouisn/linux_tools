### Recursively copy over ssh using rsync

##### as owner:

```bash
rsync -avrz -e 'ssh -p 22' sub.domain.com:/source/folder /destination/folder
```

##### with sudo:

```bash
sudo rsync -avrz -e 'ssh -p 22' sub.domain.com:/source/folder /destination/folder
```
