### Split FLAC media file using .cue file

##### as owner:

```bash
shnsplit -f inputfile.cue -o flac -t "1%n %t" inputfile.flac
```

##### with sudo:

```bash
sudo shnsplit -f inputfile.cue -o flac -t "1%n %t" inputfile.flac
```
