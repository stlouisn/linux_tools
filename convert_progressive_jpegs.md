### Convert progressive JPEGS to non-progressive

##### as owner:

```bash
find . \
    -type f \
    -name '*.jpg' -or -name '*.JPG' -or -name '*.jpeg' -or -name '*.JPEG' \
    -exec jpegoptim -f --all-normal --preserve-perms --strip-all {} \;
```

##### with sudo:

```bash
sudo find . \
    -type f \
    -name '*.jpg' -or -name '*.JPG' -or -name '*.jpeg' -or -name '*.JPEG' \
    -exec sudo jpegoptim -f --all-normal --preserve-perms --strip-all {} \;
```
