### Clean lost+found files

##### as owner:

```bash
find / -type f -name 'lost+found' -exec rm -rf {} \;
```

##### with sudo:

```bash
sudo find / -type f -name 'lost+found' -exec sudo rm -rf {} \;
```
