### Set default chmod settings

##### as owner:

```bash
find /storage/media -type d -exec chmod 755 {} \;
find /storage/media -type f -exec chmod 644 {} \;
```

##### with sudo:

```bash
sudo find /storage/media -type d -exec sudo chmod 755 {} \;
sudo find /storage/media -type f -exec sudo chmod 644 {} \;
```
