### Copy SSH keypair to remote server

##### from current user:

```bash
#!/bin/bash

REMOTE_USER=''
REMOTE_IP=''

cat ~/.ssh/id_rsa.pub | ssh ${REMOTE_USER}@${REMOTE_IP} "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys"
```

##### from other user:

```bash
LOCAL_USER=''
REMOTE_USER=''
REMOTE_IP=''

sudo cat /home/${LOCAL_USER}/.ssh/id_rsa.pub | ssh ${REMOTE_USER}@${REMOTE_IP} "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys"
```

##### from root:

```bash
REMOTE_USER=''
REMOTE_IP=''

sudo cat /root/.ssh/id_rsa.pub | ssh ${REMOTE_USER}@${REMOTE_IP} "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >> ~/.ssh/authorized_keys && chmod 600 ~/.ssh/authorized_keys"
```
