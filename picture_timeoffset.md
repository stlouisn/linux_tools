### Set Picture TimeOffSet

##### as owner:

```bash
exiftool '-DateTimeOriginal=-0000:00:00 04:00:00' -P -overwrite_original img_*
```

##### with sudo:

```bash
sudo exiftool '-DateTimeOriginal=-0000:00:00 04:00:00' -P -overwrite_original img_*
```
