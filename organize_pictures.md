### Organize Pictures

##### as owner:

```bash
# RENAME ALL FILES TO LOWERCASE
rename 'y/A-Z/a-z/' *

# REMOVE LEADING 'img_' and 'vid_'
rename 's/img_//' *
rename 's/vid_//' *

# DELETE UNNECESSARY FILES
find . -name thumbs.db -or -name '*.tmp' -exec rm {} \;

# RENAME ALL IMAGES USING EXIF DATA
exiv2 mv -F -r %Y%m%d_%H%M%S *.jpg

# FIX IMAGES MISSING EXIFDATA
exiftool '-DateTimeOriginal<FileModifyDate' -P -overwrite_original img_*
exiv2 mv -F -r %Y%m%d_%H%M%S *.jpg
```

##### with sudo:

```bash
# RENAME ALL FILES TO LOWERCASE
sudo rename 'y/A-Z/a-z/' *

# REMOVE LEADING 'img_' and 'vid_'
sudo rename 's/img_//' *
sudo rename 's/vid_//' *

# DELETE UNNECESSARY FILES
sudo find . -name thumbs.db -or -name '*.tmp' -exec sudo rm {} \;

# RENAME ALL IMAGES USING EXIF DATA
sudo exiv2 mv -F -r %Y%m%d_%H%M%S *.jpg

# FIX IMAGES MISSING EXIFDATA
sudo exiftool '-DateTimeOriginal<FileModifyDate' -P -overwrite_original img_*
sudo exiv2 mv -F -r %Y%m%d_%H%M%S *.jpg
```
