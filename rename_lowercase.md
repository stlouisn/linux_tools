### Rename Files Lowercase

##### as owner:

```bash
rename 'y/A-Z/a-z/' *
```

##### with sudo:

```bash
sudo rename 'y/A-Z/a-z/' *
```
