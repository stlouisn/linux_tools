### Generate htpasswd

```bash
docker run \
    --name htpasswd \
    --hostname htpasswd \
    --rm \
    httpd:alpine htpasswd -nb <username> <password>
```
