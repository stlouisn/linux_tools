### Backup Pictures using gzip/tar

##### using tar:

```bash
nohup tar -cvpzf /storage/pictures_`date '+%Y%m%d'`.tar.gz /storage/pictures &
```

##### using 7zip:

```bash
nohup 7z a /storage/pictures_`date '+%Y%m%d'`.7z /storage/pictures &
```
